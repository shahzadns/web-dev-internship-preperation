/**
 * Created by macbookpro on 4/25/16.
 */

(function(){

  'use strict';

  angular
    .module('basicApp')
    .controller('EventsCtrl', EventsCtrl);

  EventsCtrl.$inject = ['$scope', 'eventsFactory'];

  function EventsCtrl($scope, eventsFactory) {

    /*locals*/
    //....

    console.log('events ctrl');

    /*VM properties*/
    $scope.name = 'events';
    $scope.events = [];
    $scope.status = {
      fetching: false,
      fetchError: ''
    };


    /*VM functions*/
    $scope.getEvents = getEvents;


    /*Initialization*/
    getEvents();


    /*function declarations*/

    //fetch all events from server
    function getEvents() {

      //clear current list
      $scope.events = [];

      $scope.status.fetching = true;
      $scope.status.fetchError = '';

      eventsFactory.getEvents()
        .then(function (events) {

          $scope.status.fetching = false;
          $scope.events = events;

        }, function (reason) {

          $scope.status.fetching = false;
          $scope.status.fetchError = reason;

        });

    }
  }

})();