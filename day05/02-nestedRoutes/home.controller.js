/**
 * Created by macbookpro on 4/25/16.
 */

(function(){

  'use strict';

  angular
    .module('basicApp')
    .controller('HomeCtrl', HomeCtrl);

  HomeCtrl.$inject = ['$scope'];

  function HomeCtrl($scope) {

    $scope.name = 'home';

  }

})();