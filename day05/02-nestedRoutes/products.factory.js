/**
 * Created by macbookpro on 4/25/16.
 */

(function(){

  'use strict';

  angular
    .module('basicApp')
    .factory('productsFactory', productsFactory);

  productsFactory.$inject = ['$q', '$timeout'];

  function productsFactory($q, $timeout) {


    return {
      getProducts: getProducts,
      getProductById: getProductsById
    };

    //fetch all products from server
    function getProducts() {

      var deferred = $q.defer();

      $timeout(function () {

        deferred.resolve(getDummyProducts());

      }, 500);


      return deferred.promise;
    }

    //fetch selected product from server
    function getProductsById(productId) {
      var deferred, products;

      deferred = $q.defer();

      $timeout(function () {

        //get all products
        products = getDummyProducts();

        deferred.resolve( products[productId] );

      }, 500);


      return deferred.promise;
    }

    //returns a dummy collection of products
    function getDummyProducts() {
      var products;

      products = [];

      //create products
      for (var i = 0; i < 5; i++) {
        products.push({
          id: i,
          name: 'Product ' + (i + 1),
          price: i % 2 === 0 ? 5 : 7
        });

      }
      //console.log(products);
      return products;

    }

  }


})();