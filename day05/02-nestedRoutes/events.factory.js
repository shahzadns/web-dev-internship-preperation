/**
 * Created by macbookpro on 4/25/16.
 */

(function(){

  'use strict';

  angular
    .module('basicApp')
    .factory('eventsFactory', eventsFactory);

  eventsFactory.$inject = ['$q', '$http', '$timeout'];

  function eventsFactory($q, $http, $timeout) {


    return {
      getEvents: getEvents,
      getEventById: getEventById
    };

    //fetch all events from server
    function getEvents() {

      var deferred = $q.defer();

      $timeout(function () {

        deferred.resolve(getDummyEvents());

      }, 700);


      return deferred.promise;
    }

    //fetch selected event from server
    function getEventById(eventId) {
      var deferred, events;

      deferred = $q.defer();

      $timeout(function () {

        //get all events
        events = getDummyEvents();

        deferred.resolve( events[eventId] );

      }, 700);


      return deferred.promise;
    }

    //returns a dummy collection of events
    function getDummyEvents(){
      var events;

      events = [];

      //create events
      for (var i = 0; i < 5; i++) {
        events.push({
          id: i,
          event_num: i + 500,
          date: '25-04-2016',
          name: 'AngularJS Conf ' + (i + 1),
          city: i % 2 === 0 ? 'Karachi': 'Lahore',
          summary: 'An AngularJS conf in town',
          members: ['user1', 'user2', 'user9']
        });
      }

      return events;
    }

  }

})();