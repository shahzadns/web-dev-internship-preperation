/**
 * Created by macbookpro on 4/25/16.
 */

(function(){

  'use strict';

  angular
    .module('basicApp')
    .controller('EventsDetailsCtrl', EventsDetailsCtrl);

  EventsDetailsCtrl.$inject = ['$scope', '$state', 'eventsFactory'];

  function EventsDetailsCtrl($scope, $state, eventsFactory) {

    console.log('events details ctrl');

    /*locals*/
    var eventId;

    /*VM properties*/
    $scope.name = 'events details';
    $scope.event = undefined;
    $scope.status = {
      fetching: false,
      fetchError: ''
    };


    /*VM functions*/
    //$scope.getEventDetails = getEventDetails;


    /*Initialization*/
    //get eventId from URL
    eventId = $state.params.eventId;
    console.log(eventId);

    //get event details object
    getEventDetails();


    /*function declarations*/

    //fetch all events from server
    function getEventDetails() {

      $scope.status.fetching = true;
      $scope.status.fetchError = '';

      eventsFactory.getEventById(eventId)
        .then(function (event) {

          $scope.status.fetching = false;
          $scope.event = event;

        }, function (reason) {

          $scope.status.fetching = false;
          $scope.status.fetchError = reason;

        });

    }
  }

})();