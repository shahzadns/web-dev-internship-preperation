/**
 * Created by macbookpro on 4/25/16.
 */

(function(){

  'use strict';

  angular
    .module('basicApp')
    .config(config);

  config.$inject = ['$stateProvider', '$urlRouterProvider', '$locationProvider'];

  function config($stateProvider, $urlRouterProvider, $locationProvider) {

    //set-up app's states and their routes
    $stateProvider
      .state('home', {
        url: '/',
        templateUrl: '/awais/day05/02-nestedRoutes/home.view.html',
//          template: '<div><p>This is {{name}} page</p></div>',
        controller: 'HomeCtrl'
      })
      .state('products', {
        url: '/products',
        templateUrl: '/awais/day05/02-nestedRoutes/products.view.html',
//          template: '<div><p>This is {{name}} page</p></div>',
        controller: 'ProductsCtrl'
      })
      .state('products.details', {
        url: '/:productId',
        templateUrl: '/awais/day05/02-nestedRoutes/products.details.view.html',
        controller: 'ProductsDetailsCtrl'
      })
      .state('events', {
        url: '/events',
        templateUrl: '/awais/day05/02-nestedRoutes/events.view.html',
//          template: '<div><p>This is {{name}} page</p></div>',
        controller: 'EventsCtrl'
      })

      //output href="events/4"
      .state('events.details', {
        url: '/:eventId',
        templateUrl: '/awais/day05/02-nestedRoutes/events.details.view.html',
        controller: 'EventsDetailsCtrl'
      });


    //handle 404/unknown routes
    $urlRouterProvider.otherwise('/'); //pass the URL instead of state


    //Declare the routes as SPA for SEO
    $locationProvider.hashPrefix('!');

  }

})();
