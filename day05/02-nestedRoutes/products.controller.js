/**
 * Created by macbookpro on 4/25/16.
 */

(function(){

  'use strict';

  angular
    .module('basicApp')
    .controller('ProductsCtrl', ProductsCtrl);

  ProductsCtrl.$inject = ['$scope', 'productsFactory'];

  function ProductsCtrl($scope, productsFactory) {

    /*locals*/
    //....

    console.log('products ctrl');

    /*VM properties*/
    $scope.name = 'products';
    $scope.products = [];
    $scope.status = {
      fetching: false,
      fetchError: ''
    };


    /*VM functions*/
    $scope.getProducts = getProducts;


    /*Initialization*/
    getProducts();


    /*function declarations*/

    //fetch all products from server
    function getProducts() {

      //clear current list
      $scope.products = [];

      $scope.status.fetching = true;
      $scope.status.fetchError = '';

      productsFactory.getProducts()
        .then(function (products) {

          $scope.status.fetching = false;
          $scope.products = products;

        }, function (reason) {

          $scope.status.fetching = false;
          $scope.status.fetchError = reason;

        });

    }
  }

})();