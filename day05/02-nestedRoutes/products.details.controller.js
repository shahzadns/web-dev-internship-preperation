/**
 * Created by macbookpro on 4/25/16.
 */

(function(){

  'use strict';

  angular
    .module('basicApp')
    .controller('ProductsDetailsCtrl', ProductsDetailsCtrl);

  ProductsDetailsCtrl.$inject = ['$scope', '$state', 'productsFactory'];

  function ProductsDetailsCtrl($scope, $state, productsFactory) {

    console.log('products details ctrl');

    /*locals*/
    var productId;

    /*VM properties*/
    $scope.name = 'products details';
    $scope.product = undefined;
    $scope.status = {
      fetching: false,
      fetchError: ''
    };


    /*VM functions*/


    /*Initialization*/
    //get productId from URL
    productId = $state.params.productId;
    console.log(productId);

    //get product details object
    getProductDetails();


    /*function declarations*/

    //fetch all products from server
    function getProductDetails() {

      $scope.status.fetching = true;
      $scope.status.fetchError = '';

      productsFactory.getProductById(productId)
        .then(function (product) {

          $scope.status.fetching = false;
          $scope.product = product;

        }, function (reason) {

          $scope.status.fetching = false;
          $scope.status.fetchError = reason;

        });

    }
  }

})();