/**
 * Created by macbookpro on 4/27/16.
 */


(function(){

  var app = angular
    .module("myApp", [
      'ui.bootstrap'
    ]);

  app.controller('MainCtrl', MainCtrl);

  MainCtrl.$inject = ['$scope'];

  function MainCtrl($scope) {
    $scope.popups = {
      isDateOpened: false
    }
  }


})();