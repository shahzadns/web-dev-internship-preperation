/**
 * Created by macbookpro on 4/27/16.
 */


(function(){

  var app = angular
    .module("myApp", [
      //'ngTableModule',
      'myTableModule'
    ]);

  app.controller('ShirtsCtrl', ShirtsCtrl);
  app.controller('PentsCtrl', PentsCtrl);

  ShirtsCtrl.$inject = ['$scope'];
  PentsCtrl.$inject = ['$scope'];

  function ShirtsCtrl($scope) {
    $scope.compName = 'Shirts';
    $scope.shirts = [{
      id: 1,
      brand: 'Maaz Jee',
      size: 'Medium'
    }, {
      id: 2,
      brand: 'Maaz Jee',
      size: 'Large'
    }, {
      id: 3,
      brand: 'Dinners',
      size: 'Small'
    }, {
      id: 4,
      brand: 'Maaz Jee',
      size: 'Medium'
    }, {
      id: 5,
      brand: 'Dinners',
      size: 'Small'
    }];
  }


  function PentsCtrl($scope) {
    $scope.compName = 'Pents';
    $scope.pents = [{
      id: 1,
      brand: 'Boss',
      size: 'Medium'
    }, {
      id: 2,
      brand: 'Maaz Jee',
      size: 'Large'
    }, {
      id: 3,
      brand: 'Dinners',
      size: 'Small'
    }, {
      id: 4,
      brand: 'Boss',
      size: 'Medium'
    }, {
      id: 5,
      brand: 'Dinners',
      size: 'Small'
    }];
  }

})();