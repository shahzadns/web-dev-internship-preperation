/**
 * Created by macbookpro on 4/27/16.
 */

(function(){

  angular
    .module('ngTableModule', [])
    .directive('ngTable', ngTable);

  ngTable.$inject = [];

  function ngTable() {

    return {
      restrict: 'E',
      templateUrl: 'ngTable/ngTable.directive.view.html',
      //scope: false, //by default - uses controller's scope
      //scope: true, // to create a new and separate scope for this directive
      scope: { // creates a separate scope but takes some info from controller's scope via attributes
        records: '='
      },
      link: link
    };

    function link(scope, element, attrs) {
      console.log('ngTable');
    }
  }

})();