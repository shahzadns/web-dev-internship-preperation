/**
 * Created by macbookpro on 4/30/16.
 */

(function(){

  'use strict';

  angular
    .module('myTableModule', [])
    .directive('myTable', myTable);

  myTable.$inject = ['$rootScope'];

  function myTable($rootScope) {

    //directive definition object
    return {
      //restrict: 'A',
      //restrict: 'E',
      restrict: 'EA',
      //restrict: 'A', // AECM => attribute, element, Class, Comment

      //template: '<p>myTable directive</p>',
      templateUrl: 'myTable/myTable.directive.view.html',

      //scope: false, // default - use same scope of ctrl
      //scope: true, //child scope - creates a new separate scope and inheritates EVERYTHING from ctrl's scope
      scope: {  //child isolate scope - creates a new separate scope and inheritates GIVEN PROPERTIES from ctrl's scope

        records: '=', //two-way data-binding
        //records: '<', //one-way data-binding
        compName: '=',
        title: '@' //expression binding
      },
      link: link
    };

    function link(scope, element, attrs) {
      //console.log(scope.shirts);
      //console.log(scope.records);
      console.log('myTable init');

      /*initialization*/
      //scope.title = 'hello ' + scope.compName;
      console.log(scope.title);

      scope.records.push({
        id: 1000,
        size: 'Super large',
        brand: 'Crazy Brand'
      });
    }
  }
})();