
/*task execution*/

//printTable(7);
//printFooBarBaz();
getStatus();


/*task: print table of n*/ //.e.g 5, 6, 7
function printTable( n ){

  for (var i = 1; i <= 10; i++) {
    console.log (n + ' * ' + i + ' = ' + n * i);
  }

}

/*task: iterate over 20 counting. print foo if divisible by 3, 'bar' if divisible by 5, and 'baz' if neither of these.*/

function printFooBarBaz() {

  for (var i = 1; i <= 20; i++) {

    if (i % 3 == 0) {
      console.log(' foo ', i);
    }
    if (i % 5 == 0) {
      console.log(' bar ', i);
    }
    if (i % 5 != 0 && i % 3 != 0 ) {
      console.log(' baz ', i);
    }

  }

}


//var messages = {
//  'foo' : {
//    message: 'hello my firend.',
//    time: '7:51pm'
//  },
//  'bar': {
//    message: 'Hi how are you doing ?.',
//    time: '7:53pm'
//  }
//};
//
//var user = 'baz';
//
//console.log( messages[user] ? messages[user].message : 'unknown user');


/*task: keep 4 status against user-names, prompt user to the username, and alert his status.
 if doesn't match then print "unknown username"*/

function getStatus() {
  var statuses, name, status;

  statuses = {
    'awais': {
      status: 'he is learning'
    },
    'shahzad': {
      status: 'he is teaching'
    },
    'adeel': {
      status: 'taking to customer'
    },
    'taha': {
      status: 'he is not here'
    }
  };

  name = prompt('Please enter your name.');

  //status = statuses[name];
  status = statuses[name] ? statuses[name].status : 'unknown username';

  //this breaks the code because 'status of undefined".
  //status = statuses[name].status || 'unknown username';

  console.log(status);


}




