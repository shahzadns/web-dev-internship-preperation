(function () {
    var app = angular.
        module('myApp', []);

    app.controller('ShirtsCtrl', ShirtsCtrl);
    app.controller('PentsCtrl', PentsCtrl);

    ShirtsCtrl.$inject = ['$scope'];
    PentsCtrl.$inject = ['$scope'];

    function ShirtsCtrl($scope) {

        $scope.shirts = [{
                id: 1,
                brand: 'MaazJee',
                size: 'small'
            }, {
                id: 2,
                brand: 'MaazJee',
                size: 'medium'
            }, {
                id: 3,
                brand: 'MaazJee',
                size: 'large'
            }]
    }

    function PentsCtrl($scope) {

        $scope.pents=[{
            id: 1,
            brand: 'EdenRob',
            size: 'small'
        }, {
            id: 2,
            brand: 'EdenRob',
            size: 'medium'
        }, {
            id: 3,
            brand: 'EdenRob',
            size: 'large'
        }]
    }


})();

//
//(function () {
//
//    var app = angular
//        .module("myApp", [
////            'ngTableModule'
//        ]);
//
//    app.controller('ShirtsCtrl', ShirtsCtrl);
//    app.controller('PentsCtrl', PentsCtrl);
//
//    ShirtsCtrl.$inject = ['$scope'];
//    PentsCtrl.$inject = ['$scope'];
//
//    function ShirtsCtrl($scope) {
//        $scope.shirts = [
//            {
//                id: 1,
//                brand: 'Maaz Jee',
//                size: 'Medium'
//            },
//            {
//                id: 2,
//                brand: 'Maaz Jee',
//                size: 'Large'
//            },
//            {
//                id: 3,
//                brand: 'Dinners',
//                size: 'Small'
//            },
//            {
//                id: 4,
//                brand: 'Maaz Jee',
//                size: 'Medium'
//            },
//            {
//                id: 5,
//                brand: 'Dinners',
//                size: 'Small'
//            }
//        ];
//    }
//
//
//    function PentsCtrl($scope) {
//        $scope.pents = [
//            {
//                id: 1,
//                brand: 'Boss',
//                size: 'Medium'
//            },
//            {
//                id: 2,
//                brand: 'Maaz Jee',
//                size: 'Large'
//            },
//            {
//                id: 3,
//                brand: 'Dinners',
//                size: 'Small'
//            },
//            {
//                id: 4,
//                brand: 'Boss',
//                size: 'Medium'
//            },
//            {
//                id: 5,
//                brand: 'Dinners',
//                size: 'Small'
//            }
//        ];
//    }
//
//})();
///**
// * Created by NTS-PC-37 on 4/28/2016.
// */
