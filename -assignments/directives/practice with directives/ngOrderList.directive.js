(function(){

    angular
        .module('ngOrderListModule', [])
        .directive('ngOrderList', ngOrderList);

    ngOrderList.$inject = [];

    function ngOrderList() {

        return {
            restrict: 'C',
            templateUrl: './ngOrderList.directive.view.html',
            //scope: false, //by default - uses controller's scope
            //scope: true, // to create a new and separate scope for this directive
            scope: { // creates a separate scope but takes some info from controller's scope via attributes
                records: '='
            },
            link: link
        };

        function link(scope, element, attrs) {
            console.log('ngOrderList');
        }
    }

})();